# Hello world using Google Kubernetes Engine

This is a simple app to demonstrate a DevOps build, test, and deployment pipeline - node js server, version controlled by gitlab, hosted on GKE, monitored by Prometheus.
